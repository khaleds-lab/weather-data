package de.exxcellent.challenge.service;

import de.exxcellent.challenge.entity.Weather;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WeatherService extends SpreadService<Weather> {

    public WeatherService(final List<Weather> weatherDataOfOneMonth) {
        super(weatherDataOfOneMonth);
    }

    @Override
    public String getSmallestSpread() {
        return String.valueOf(this.findDayWithSmallestTempSpread());
    }

    private int findDayWithSmallestTempSpread() {
        List<Integer> weatherDataOfTempSpread = this.getAllTempSpread(super.dataFromFileParser);

        int minTempSpread = Collections.min(weatherDataOfTempSpread);
        int indexOfDayWithMinTempSpread = weatherDataOfTempSpread.indexOf(minTempSpread);

        return super.dataFromFileParser
                .get(indexOfDayWithMinTempSpread)
                .getDay();
    }

    private List<Integer> getAllTempSpread(final List<Weather> weatherDataOfOneMonth) {
        return weatherDataOfOneMonth
                .stream()
                .map(day -> day.getMaxTemp() - day.getMinTemp())
                .collect(Collectors.toList());
    }

}
