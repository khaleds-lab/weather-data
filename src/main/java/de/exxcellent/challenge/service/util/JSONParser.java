package de.exxcellent.challenge.service.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class JSONParser implements Parser {

    private final String file;

    private final Class<?> classType;

    public <T> JSONParser(final String file, final Class<T> classType) {
        this.file = file;
        this.classType = classType;
    }

    @Override
    public <T> List<T> parseFileToList() throws IOException {
        if (this.file == null || this.file.isEmpty()) {
            throw new IllegalArgumentException("File directory is not found");
        }
        if (this.classType == null) {
            throw new IllegalArgumentException("The type of the bean Class is null");
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper
                .readValue(new FileReader(this.file), mapper.getTypeFactory()
                        .constructCollectionType(List.class, this.classType));
    }

}
