package de.exxcellent.challenge.service.util;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class CSVParser implements Parser {

    private final String file;

    private final Class<?> classType;

    public <T> CSVParser(final String file, final Class<T> classType) {
        this.file = file;
        this.classType = classType;
    }

    @Override
    public <T> List<T> parseFileToList() throws FileNotFoundException {
        if (this.file == null || this.file.isEmpty()) {
            throw new IllegalArgumentException("File directory is not found");
        }
        if (this.classType == null) {
            throw new IllegalArgumentException("The type of the bean Class is null");
        }
        return new CsvToBeanBuilder(new FileReader(this.file))
                .withType(this.classType)
                .build()
                .parse();
    }

}
