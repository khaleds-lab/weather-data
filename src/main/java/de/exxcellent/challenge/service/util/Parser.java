package de.exxcellent.challenge.service.util;

import java.io.IOException;
import java.util.List;

public interface Parser {

    <T> List<T> parseFileToList() throws IOException;
}
