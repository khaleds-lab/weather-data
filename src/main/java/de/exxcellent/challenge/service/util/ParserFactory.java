package de.exxcellent.challenge.service.util;

public class ParserFactory {

    public static <T> Parser getFileParser(final String file, final Class<T> classType) {

        if (file.endsWith(".csv")) {
            return new CSVParser(file, classType);

        } else if (file.endsWith(".json")) {
            return new JSONParser(file, classType);

        } else {
            throw new IllegalArgumentException("File Type cannot be parsed.");
        }
    }

}
