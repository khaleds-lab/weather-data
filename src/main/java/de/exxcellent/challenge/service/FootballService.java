package de.exxcellent.challenge.service;

import de.exxcellent.challenge.entity.Football;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FootballService extends SpreadService<Football> {

    public FootballService(final List<Football> englishPremierLeagueResults) {
        super(englishPremierLeagueResults);
    }

    @Override
    public String getSmallestSpread() {
        return this.findTeamWithSmallestGoalSpread();
    }

    private String findTeamWithSmallestGoalSpread() {
        List<Integer> resultsOfAllGoalSpreads = this.getAllGoalSpread(super.dataFromFileParser);

        int minGoalSpread = Collections.min(resultsOfAllGoalSpreads);
        int indexOfTeamWithMinGoalSpread = resultsOfAllGoalSpreads.indexOf(minGoalSpread);

        return super.dataFromFileParser
                .get(indexOfTeamWithMinGoalSpread)
                .getTeam();
    }

    private List<Integer> getAllGoalSpread(final List<Football> englishPremierLeagueResults) {
        return englishPremierLeagueResults
                .stream()
                .map(team -> Math.abs(team.getGoals() - team.getGoalsAllowed()))
                .collect(Collectors.toList());
    }

}
