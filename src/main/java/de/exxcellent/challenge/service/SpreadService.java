package de.exxcellent.challenge.service;

import java.util.List;

public abstract class SpreadService<T> {

    protected List<T> dataFromFileParser;

    public SpreadService(List<T> dataFromFileParser) {

        if (dataFromFileParser == null || dataFromFileParser.isEmpty()) {
            throw new IllegalArgumentException("Value of the List is null.");
        }
        this.dataFromFileParser = dataFromFileParser;
    }

    public abstract String getSmallestSpread();

}
