package de.exxcellent.challenge;

import de.exxcellent.challenge.entity.Football;
import de.exxcellent.challenge.entity.Weather;
import de.exxcellent.challenge.service.*;
import de.exxcellent.challenge.service.util.Parser;
import de.exxcellent.challenge.service.util.ParserFactory;

import java.io.IOException;

/**
 * The entry class for your solution. This class is only aimed as starting point and not intended as baseline for your software
 * design. Read: create your own classes and packages as appropriate.
 *
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public final class App {

    /**
     * This is the main entry method of your program.
     *
     * @param args The CLI arguments passed
     */
    public static void main(String... args) throws IOException {

        // Your preparation code …
        String weatherCSVFile = "src/main/resources/de/exxcellent/challenge/weather.csv";
        String weatherJSONFile = "src/main/resources/de/exxcellent/challenge/weather.json";
        String footballCSVFile = "src/main/resources/de/exxcellent/challenge/football.csv";
        String footballJSONFile = "src/main/resources/de/exxcellent/challenge/football.json";

        Parser fileParser;

        fileParser = ParserFactory.getFileParser(weatherCSVFile, Weather.class);
        SpreadService weatherCsv = new WeatherService(fileParser.parseFileToList());

        fileParser = ParserFactory.getFileParser(weatherJSONFile, Weather.class);
        SpreadService weatherJson = new WeatherService(fileParser.parseFileToList());

        fileParser = ParserFactory.getFileParser(footballCSVFile, Football.class);
        SpreadService footballCsv = new FootballService(fileParser.parseFileToList());

        fileParser = ParserFactory.getFileParser(footballJSONFile, Football.class);
        SpreadService footballJson = new FootballService(fileParser.parseFileToList());

        String dayWithSmallestTempSpreadCSV = weatherCsv.getSmallestSpread();
        System.out.printf("Day with smallest temperature spread from csv file : %s%n", dayWithSmallestTempSpreadCSV);

        String dayWithSmallestTempSpreadJSON = weatherJson.getSmallestSpread();
        System.out.printf("Day with smallest temperature spread from json file: %s%n", dayWithSmallestTempSpreadJSON);

        String teamWithSmallestGoalSpreadCSV = footballCsv.getSmallestSpread();
        System.out.printf("Team with smallest goal spread from csv file       : %s%n", teamWithSmallestGoalSpreadCSV);

        String teamWithSmallestGoalSpreadJSON = footballJson.getSmallestSpread();
        System.out.printf("Team with smallest goal spread from json file      : %s%n", teamWithSmallestGoalSpreadJSON);
    }
}
