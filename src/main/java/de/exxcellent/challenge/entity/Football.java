package de.exxcellent.challenge.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Football {

    @CsvBindByName(column = "Team")
    @JsonProperty(value = "Team")
    private String team;

    @CsvBindByName(column = "Goals")
    @JsonProperty(value = "Goals")
    private int goals;

    @CsvBindByName(column = "Goals Allowed")
    @JsonProperty(value = "Goals Allowed")
    private int goalsAllowed;

    public String getTeam() {
        return team;
    }

    public int getGoals() {
        return goals;
    }

    public int getGoalsAllowed() {
        return goalsAllowed;
    }

    @Override
    public String toString() {
        return "Football{" +
                "team='" + team + '\'' +
                ", goals='" + goals + '\'' +
                ", goalsAllowed='" + goalsAllowed + '\'' +
                '}';
    }

}
