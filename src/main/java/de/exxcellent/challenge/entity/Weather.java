package de.exxcellent.challenge.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

    @CsvBindByName(column = "Day")
    @JsonProperty(value = "Day")
    private int day;

    @CsvBindByName(column = "MxT")
    @JsonProperty(value = "MxT")
    private int maxTemp;

    @CsvBindByName(column = "MnT")
    @JsonProperty(value = "MnT")
    private int minTemp;

    public int getDay() {
        return day;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public int getMinTemp() {
        return minTemp;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "day=" + day +
                ", maxTemp=" + maxTemp +
                ", minTemp=" + minTemp +
                '}';
    }

}
