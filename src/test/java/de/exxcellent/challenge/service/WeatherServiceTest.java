package de.exxcellent.challenge.service;

import de.exxcellent.challenge.entity.Weather;
import de.exxcellent.challenge.service.util.Parser;
import de.exxcellent.challenge.service.util.ParserFactory;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Testing implementation of {@link de.exxcellent.challenge.service.WeatherService}
 *
 * @author Khaled Ahmed
 */
class WeatherServiceTest {

    private SpreadService weatherService;

    private Parser parser;

    private String filePath = "src/main/resources/de/exxcellent/challenge/";

    private String dayWithSmallestTempSpread = "14";

    @Test
    void testGetDayWithSmallestTempSpread_fromCSVFile() throws IOException {
        //prepare
        filePath += "weather.csv";
        this.parser = ParserFactory.getFileParser(filePath, Weather.class);
        this.weatherService = new WeatherService(parser.parseFileToList());

        //test
        String dayWithSmallestTempSpreadFromCSVFile = this.weatherService.getSmallestSpread();

        //assert
        assertEquals(dayWithSmallestTempSpreadFromCSVFile, dayWithSmallestTempSpread);
    }

    @Test
    void testGetDayWithSmallestTempSpread_fromJSONFile() throws IOException {
        //prepare
        filePath += "weather.json";
        this.parser = ParserFactory.getFileParser(filePath, Weather.class);
        this.weatherService = new WeatherService(parser.parseFileToList());

        //test
        String dayWithSmallestTempSpreadFromJSONFile = this.weatherService.getSmallestSpread();

        //assert
        assertEquals(dayWithSmallestTempSpreadFromJSONFile, dayWithSmallestTempSpread);
    }

    @Test
    void testWeatherService_nullList_error() {
        assertThrows(IllegalArgumentException.class, () -> new WeatherService(null));
    }

    @Test
    void testWeatherService_emptyList_error() {
        assertThrows(IllegalArgumentException.class, () -> new WeatherService(Collections.emptyList()));
    }

}