package de.exxcellent.challenge.service.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing implementation of {@link de.exxcellent.challenge.service.util.ParserFactory}
 *
 * @author Khaled Ahmed
 */
class ParserFactoryTest {

    @Test
    void testGetCSVParser() {
        Parser parser = ParserFactory.getFileParser(".csv", Object.class);

        assertTrue(parser instanceof CSVParser);
    }

    @Test
    void testGetJSONParser() {
        Parser parser = ParserFactory.getFileParser(".json", Object.class);

        assertTrue(parser instanceof JSONParser);
    }

    @Test
    void testParser_error() {
        assertThrows(IllegalArgumentException.class,
                () -> ParserFactory.getFileParser(".jpeg", Object.class));
    }

}