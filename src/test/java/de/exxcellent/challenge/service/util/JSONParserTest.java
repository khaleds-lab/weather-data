package de.exxcellent.challenge.service.util;

import de.exxcellent.challenge.entity.Weather;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Testing implementation of {@link de.exxcellent.challenge.service.util.JSONParser}
 *
 * @author Khaled Ahmed
 */
class JSONParserTest {

    @Test
    void testJSONParse_ok() throws IOException {
        //prepare
        String filePath = "src/main/resources/de/exxcellent/challenge/weather.json";

        //test
        List<Weather> weatherListOfOneMonth = ParserFactory
                .getFileParser(filePath, Weather.class)
                .parseFileToList();

        //assert
        assertEquals(30, weatherListOfOneMonth.size());
    }

    @Test
    void testParseNullFile_error() {
        assertThrows(IllegalArgumentException.class,
                () -> new JSONParser(null, Weather.class).parseFileToList());
    }

    @Test
    void testParseEmptyFile_error() {
        assertThrows(IllegalArgumentException.class,
                () -> new JSONParser("", Weather.class).parseFileToList());
    }

    @Test
    void testParseNullClass_error() {
        //prepare
        String filePath = "src/main/resources/de/exxcellent/challenge/weather.json";

        //test & assert
        assertThrows(IllegalArgumentException.class,
                () -> new JSONParser(filePath, null).parseFileToList());
    }
}