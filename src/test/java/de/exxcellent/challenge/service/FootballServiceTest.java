package de.exxcellent.challenge.service;

import de.exxcellent.challenge.entity.Football;
import de.exxcellent.challenge.service.util.Parser;
import de.exxcellent.challenge.service.util.ParserFactory;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Testing implementation of {@link de.exxcellent.challenge.service.FootballService}
 *
 * @author Khaled Ahmed
 */
class FootballServiceTest {

    private SpreadService footballService;

    private Parser parser;

    private String filePath = "src/main/resources/de/exxcellent/challenge/";

    private String teamWithSmallestGoalSpread = "Aston_Villa";

    @Test
    void testGetTeamWithSmallestGoalSpread_fromCSVFile() throws IOException {
        //prepare
        filePath += "football.csv";
        this.parser = ParserFactory.getFileParser(filePath, Football.class);
        this.footballService = new FootballService(parser.parseFileToList());

        //test
        String teamWithSmallestGoalSpreadFromCSVFile = this.footballService.getSmallestSpread();

        //assert
        assertEquals(teamWithSmallestGoalSpreadFromCSVFile, teamWithSmallestGoalSpread);
    }

    @Test
    void testGetTeamWithSmallestGoalSpread_fromJSONFile() throws IOException {
        //prepare
        filePath += "football.json";
        parser = ParserFactory.getFileParser(filePath, Football.class);
        this.footballService = new FootballService(parser.parseFileToList());

        //test
        String teamWithSmallestGoalSpreadFromJSONFile = this.footballService.getSmallestSpread();

        //assert
        assertEquals(teamWithSmallestGoalSpreadFromJSONFile, teamWithSmallestGoalSpread);
    }

    @Test
    void testFootballService_nullList_error() {
        assertThrows(IllegalArgumentException.class, () -> new FootballService(null));
    }

    @Test
    void testFootballService_emptyList_error() {
        assertThrows(IllegalArgumentException.class, () -> new FootballService(Collections.emptyList()));
    }

}